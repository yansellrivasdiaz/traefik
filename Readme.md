Traefik
=======

![](https://d33wubrfki0l68.cloudfront.net/1b8ea408142c253bb8e16596218e4e328d019c58/862c3/assets/img/traefik.logo.bright@3x.svg)

This is the reverse proxy used to handle incoming requests.

##### Requirements  

1. `docker` with `docker-compose`
2. Ports `80`, `8080`, and `443` should be available for `Traefik` to use

##### Installation  

1. Clone this repository

```bash
$ git clone https://yansellrivasdiaz@bitbucket.org/yansellrivasdiaz/traefik.git	 
```

2. Start the services

```bash
$ cd traefik
$ docker-compose up -d
```	
	
3. Traefik will be available on port `8080`


## Using SSL, generated with [mkcert](https://github.com/FiloSottile/mkcert)

1. Copy `.env.sample` to `.env` or echo the value.
```bash
$ echo "DOMAIN_NAME=name.local" > .env
```

2. Install the `mkcert` utils and generate the certs
```bash
$ make install-mkcert
$ make certs
$ docker-compose down && docker-compose up -d
```

`Note: ` each docker container that you want to be pusblished on traefik need to include the next labels.
```yml
traefik.frontend.rule=Host:service.domain.local
traefik.enable=true
```

##### Configuration

In order to perform additional configuration of the `.toml` files and autoregistration for containers see the [official documentation](https://docs.traefik.io/)


```
├── docker-compose.yml
├── etc 
│   ├── config                       # Custom configs directory
│   │   └── service.toml.example     
│   ├── ssl                          # SSL Certs
│   │   ├── cert.pem
│   │   └── key.pem
│   └── traefik.toml
├── logs                             # Where traefik will puts logs files.
│   ├── access.log
│   └── traefik.log
├── Makefile
└── Readme.md
```
