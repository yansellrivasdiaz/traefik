.PHONY: default test help

default: help

include .env

# COLORS
GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
WHITE  := $(shell tput -Txterm setaf 7)
RESET  := $(shell tput -Txterm sgr0)

TARGET_MAX_CHAR_NUM=20

## Show this help.
help:
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  ${YELLOW}%-$(TARGET_MAX_CHAR_NUM)s${RESET} ${GREEN}%s${RESET}\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

## Create SSL certs    -> [mkcerts <domain.com>]
certs:
	@echo Generating certs with *.${DOMAIN_NAME}
	@mkcert -cert-file etc/ssl/cert.pem -key-file etc/ssl/key.pem *.${DOMAIN_NAME}

## Start de service    -> [docker-compose up -d]
up:
	@docker-compose up -d

## Install `mkcert` certs utils
install-mkcert:
	@echo Installing mkcert utils on /usr/local/bin/
	@sudo curl -L https://github.com/FiloSottile/mkcert/releases/download/v1.3.0/mkcert-v1.3.0-linux-amd64 --output /usr/local/bin/mkcert --silent
	@echo Setting 'mkcert' as executable.
	@sudo chmod +x /usr/local/bin/mkcert
